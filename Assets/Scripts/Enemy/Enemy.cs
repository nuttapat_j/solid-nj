using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : EnemyBase, IGiveScore
{
    [SerializeField] private int score = 1;

    private void OnEnable()
    {
        currentHp = 5;
    }

    protected override void Death(GameObject _source)
    {
        gameObject.SetActive(false);
        
        if (GameplayManager.Instance.TryGetManager<RespawnManager>(out var _respawnManager))
        {
            _respawnManager.StartRespawn(gameObject);
        }
        
        if (_source.TryGetComponent<Player>(out _))
        {
            AddScore(score);
        }
    }

    public void AddScore(int _score)
    {
        ScoreManager.Instance.AddScore(_score);
    }
}
